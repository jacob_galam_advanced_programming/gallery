#pragma once
#include <list>
#include <io.h>
#include <iostream>
#include <vector>

#include "sqlite3.h"
#include "Album.h"
#include "User.h"

using std::string;
using Record = std::vector<string>;
using Records = std::vector<Record>;

class IDataAccess
{
public:
	virtual ~IDataAccess() = default;

	// add user id fix
	// Top tagged picture

	// album related
	virtual const std::list<Album> getAlbums();
	virtual const std::list<Album> getAlbumsOfUser(const User& user);
	virtual void createAlbum(const Album& album);
	virtual void deleteAlbum(const std::string& albumName, int userId);
	virtual bool doesAlbumExists(const std::string& albumName, int userId);
	virtual Album openAlbum(const std::string& albumName);
	virtual void closeAlbum(Album& pAlbum);
	virtual void printAlbums();

    // picture related
	virtual void addPictureToAlbumByName(const std::string& albumName, const Picture& picture);
	virtual void removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName); //
	virtual void tagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId);  // work
	virtual void untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId);  //

	// tags list
	//

	// user related
	virtual void printUsers();
	virtual User getUser(int userId);
	virtual void createUser(User& user );
	virtual void deleteUser(const User& user);
	virtual bool doesUserExists(int userId);
	

	// user statistics
	virtual int countAlbumsOwnedOfUser(const User& user);
	virtual int countAlbumsTaggedOfUser(const User& user);
	virtual int countTagsOfUser(const User& user);
	virtual float averageTagsPerAlbumOfUser(const User& user);

	// queries
	virtual User getTopTaggedUser();
	virtual Picture getTopTaggedPicture();
	virtual std::list<Picture> getTaggedPicturesOfUser(const User& user);
	
	virtual bool open();
	virtual void close();
	virtual void clear();

private:
	sqlite3* _db;

	Records selectStmt(const char* stmt);
	static void checkError(const char* comment, char* errMessage);
	static string getAlbumId(int ownerId, string name);
	static int selectCallback(void* pData, int numFields, char** pFields, char** pColNames);
	static Picture recToPic(Record rec);

	const int fastExce(const char* comment);

	void smartInsert(string tableName, string col, string data);
	void updateValue(string table, string col, string oldValue, string newValue);
	void deleteDataW(string table, string whereQ);
	
	static string searchPicFromAlbum(const std::string& albumName, const std::string& pictureName);
	
	const std::list<Album> callCommentAlbum(string comment);
	const std::list<User> callCommentUser(string comment);
};

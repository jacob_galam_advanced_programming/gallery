#include "IDataAccess.h"

#define DATABASE_INIT "CREATE TABLE IF NOT EXISTS USERS(ID INTEGER PRIMARY KEY, NAME text); CREATE TABLE IF NOT EXISTS ALBUMS(ID INTEGER PRIMARY KEY, NAME text,CREATION_DATE text,USER_ID INTEGER, FOREIGN KEY(USER_ID) REFERENCES USERS(ID)); CREATE TABLE IF NOT EXISTS PICTURES(ID INTEGER PRIMARY KEY, NAME text, LOCATION text, CREATION_DATE text, ALBUM_ID INTEGER, FOREIGN KEY(ALBUM_ID) REFERENCES ALBUMS(ID)); CREATE TABLE IF NOT EXISTS TAGS(ID INTEGER PRIMARY KEY, PICTURE_ID INTEGER, USER_ID INTEGER, FOREIGN KEY(PICTURE_ID) REFERENCES PICTURES(ID), FOREIGN KEY(USER_ID) REFERENCES USERS(ID));"

// TODO:
// check the stats if they are real
// check if the 'real delete' work
// refactor the code
// docs


/*
Open the DB
Input: None
Output: If can
*/
bool IDataAccess::open()
{
	string dbFileName = "MyDB.sqlite";
	int doesFileExist = _access(dbFileName.c_str(), 0);
	int res = sqlite3_open(dbFileName.c_str(), &this->_db);
	if (res != SQLITE_OK)
	{
		this->_db = nullptr;
		std::cout << "Failed to open DB\n";
		return false;
	}

	char* errMessage = nullptr;

	sqlite3_exec(this->_db, DATABASE_INIT, nullptr, nullptr, &errMessage);

	checkError(DATABASE_INIT, errMessage);

	return true;
}
/*
Close the DB
Input: None
Output: None
*/
void IDataAccess::close()
{
	this->_db = nullptr;
	sqlite3_close(this->_db);
}
/*
Get albums from the DB
Input: The comment
Output: List of the return albums
*/
const std::list<Album> IDataAccess::callCommentAlbum(string comment)
{
	std::list<Album> albums;

	for (Record record : selectStmt(comment.c_str()))
	{
		albums.emplace_back(std::stoi(record[3]), record[1], record[2]);
	}
	return albums;
}
/*
Get users from the DB
Input: The comment
Output: List of the return users
*/
const std::list<User> IDataAccess::callCommentUser(string comment)
{
	std::list<User> albums;

	for (Record record : selectStmt(comment.c_str()))
	{
		albums.emplace_back(std::stoi(record[0]), record[1]);
	}
	return albums;
}
/*
Check if have error and print it
Input: The comment and the return error message
Output: None
*/
void IDataAccess::checkError(const char* comment, char* errMessage)
{
	if (errMessage)
	{
		std::cout << "the comment: " << '"' << comment << '"' << " cause the error: " << '"' << errMessage << '"' << "\n";
		sqlite3_free(errMessage);
	}
}
/*
?
Input: None
Output: None
*/
void IDataAccess::clear()
{
	// TO DO what?
}
/*
Get all the albums from the DB
Input: None
Output: List of the return albums
*/
const std::list<Album> IDataAccess::getAlbums()
{
	return callCommentAlbum("SELECT * FROM ALBUMS");
}
/*
Get all the albums of user from the DB
Input: user
Output: List of the return albums
*/
const std::list<Album> IDataAccess::getAlbumsOfUser(const User& user)
{
	return callCommentAlbum("SELECT * FROM ALBUMS where USER_ID=" + std::to_string(user.getId()) + ";");
}
/*
Create new album to DB
Input: The new album
Output: None
*/
void IDataAccess::createAlbum(const Album& album)
{
	string comment = "INSERT INTO ALBUMS (NAME, USER_ID, CREATION_DATE) select '" + album.getName() + "', " + std::to_string(album.getOwnerId()) + ", '" + album.getCreationDate() + "'";
	this->fastExce(comment.c_str());
}
/*
Delete album from DB
Input: Album name and userID
Output: None
*/
void IDataAccess::deleteAlbum(const std::string& albumName, int userId)
{
	string whereComment = "USER_ID=" + std::to_string(userId) + " AND ";
	whereComment += "NAME='" + albumName + "'";

	// delete pic

	auto getPicsComment = "select NAME from PICTURES where ALBUM_ID=" + getAlbumId(userId, albumName) + ";";

	for (Record picName : this->selectStmt(getPicsComment.c_str()))
	{
		this->removePictureFromAlbumByName(albumName, picName[0]);
		
	}

	this->deleteDataW("ALBUMS", whereComment);
}
/*
Return the comment that give album id
Input: Album name and userID
Output: Comment\part of comment
*/
string IDataAccess::getAlbumId(int ownerId, string name)
{
	return "(select ID from ALBUMS where USER_ID=" + std::to_string(ownerId) + " AND NAME='" + name + "')";
}
/*
Return if album exists
Input: If exists
Output: Album name and userID
*/
bool IDataAccess::doesAlbumExists(const std::string& albumName, int userId)
{
	string comment = "SELECT * FROM ALBUMS where USER_ID=" + std::to_string(userId) + " AND " += "NAME='" + albumName + "';";
	return callCommentAlbum(comment.c_str()).size() != 0;
}
/*
Return album from DB
Input: Album name
Output: The album
*/
Album IDataAccess::openAlbum(const std::string& albumName)
{
	auto returnAlbum = callCommentAlbum("select * from ALBUMS where name='" + albumName + "' ;").front();

	auto ownerId = returnAlbum.getOwnerId();

	string getPictureSCommnet = "select * from pictures WHERE album_id = " + getAlbumId(ownerId, albumName) + ";";
	for (Record picRec : selectStmt(getPictureSCommnet.c_str()))
	{

		Picture pic = recToPic(picRec);

		string getTagComment = "select * from tags where picture_id=" + std::to_string(pic.getId()) + ";";
		for (Record tagRec : selectStmt(getTagComment.c_str()))
		{
			pic.tagUser(std::stoi(tagRec[2]));
		}
		

		returnAlbum.addPicture(pic);
	}
	return returnAlbum;
}
/*
?
Input: Album
Output: None
*/
void IDataAccess::closeAlbum(Album& pAlbum)
{
	// TODO: WTF?! close what?!
	

	//closeAlbum(pAlbum);
}
/*
Run the comment and check for error
Input: The comment
Output: The return int of exec
*/
const int IDataAccess::fastExce(const char* comment)
{
	char* errMessage = nullptr;
	int returnINT = sqlite3_exec(this->_db, comment, nullptr, nullptr, &errMessage);

	checkError(comment, errMessage);
	
	return returnINT;
}

/*
Insert the data one time to the sql
Input: db, tableName, colName and the data
Output: None
*/
void IDataAccess::smartInsert(string tableName, string col, string data)
{
	string comment = "INSERT INTO " + tableName + "(" + col + ") SELECT '" + data + "' WHERE NOT EXISTS(SELECT 1 FROM " + tableName + " WHERE " + col + " = '" + data + "');";
	this->fastExce(comment.c_str());
}
/*
Update the value to new value
Input: Table and col name, old value and the new value
Output: None
*/
void IDataAccess::updateValue(string table, string col, string oldValue, string newValue)
{
	string comment = "UPDATE " + table + " set " + col + "='" + newValue + "' where " + col + "='" + oldValue + "';";
	this->fastExce(comment.c_str());
}
/*
Delete from the DB
Input: Table name and where comment
Output: None
*/
void IDataAccess::deleteDataW(string table, string whereQ)
{
	string comment = "DELETE FROM " + table + " where " + whereQ + ";";
	this->fastExce(comment.c_str());
}
/*
Print all the albums
Input: None
Output: None
*/
void IDataAccess::printAlbums()
{
	for (Album album : this->getAlbums())
	{
		std::cout << "Name: " << album.getName() << " Creation date:" << album.getCreationDate() << " Owner id:" << album.getOwnerId() << "\n";
	}
}

/*
Add new pic to DB
Input: Album name name and the pic
Output: None
*/
void IDataAccess::addPictureToAlbumByName(const std::string& albumName, const Picture& picture)
{
	auto returnAlbum = callCommentAlbum("select * from ALBUMS where name='" + albumName + "' ;").front();

	string comment = "INSERT INTO PICTURES (NAME, LOCATION ,ALBUM_ID, CREATION_DATE) select '" + picture.getName() + "', '" + picture.getPath() + "'," + getAlbumId(returnAlbum.getOwnerId(), returnAlbum.getName()) + ", '" + picture.getCreationDate() + "';";

	this->fastExce(comment.c_str());
}
/*
Return the comment that search pic
Input: Album name and picture name
Output: Comment\part of comment
*/
string IDataAccess::searchPicFromAlbum(const std::string& albumName, const std::string& pictureName)
{
	return "PICTURES.NAME='" + pictureName + "' AND PICTURES.ALBUM_ID=(select ID from ALBUMS where ALBUMS.NAME='" + albumName + "')";
}
/*
Remove pic form DB
Input: Album name and picture name
Output: None
*/
void IDataAccess::removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName)
{
	
	auto getIDsComment = "SELECT USER_ID FROM TAGS where picture_id=(select id from pictures where album_id=( select ID from albums where name='" + albumName + "') AND NAME='" + pictureName + "');";
	
	for (Record idRecord : this->selectStmt(getIDsComment.c_str()))
	{
		untagUserInPicture(albumName, pictureName, std::stoi(idRecord[0]));
	}
	this->deleteDataW("PICTURES", searchPicFromAlbum(albumName, pictureName));
}
/*
Tag user
Input: Album name and picture name and user ID
Output: None
*/
void IDataAccess::tagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
	
	string comment = "INSERT INTO TAGS (PICTURE_ID, USER_ID) select (select ID from PICTURES where " + searchPicFromAlbum(albumName, pictureName) + "), " + std::to_string(userId) + ";";
	this->fastExce(comment.c_str());
}
/*
Untag user
Input: Album name and picture name and user ID
Output: None
*/
void IDataAccess::untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
	this->deleteDataW("TAGS", "TAGS.PICTURE_ID=( select ID from PICTURES where " + searchPicFromAlbum(albumName, pictureName) + ") AND TAGS.USER_ID=" + std::to_string(userId));
}
/*
Print all users
Input: None
Output: None
*/
void IDataAccess::printUsers()
{
	for (User user : this->callCommentUser("select * from users;"))
	{
		std::cout << "Name: " << user.getName() << " ID: " << std::to_string(user.getId());
	}
}
/*
Get user
Input: user ID
Output: User
*/
User IDataAccess::getUser(int userId)
{
	return this->callCommentUser("select * from users where id = " + std::to_string(userId) + ";").front();
}
/*
Create new user
Input: User
Output: None
*/
void IDataAccess::createUser(User& user)
{
	string comment = "INSERT INTO USERS (NAME) VALUES ('" + user.getName() + "');";
	this->fastExce(comment.c_str());
}
/*
delete user
Input: User
Output: None
*/
void IDataAccess::deleteUser(const User& user)
{
	for (auto pic : this->getTaggedPicturesOfUser(user))
	{
		string getAlbumComment = "SELECT NAME FROM ALBUMS where id=" + pic.getAlbumID() + ";";
		
		this->untagUserInPicture(this->selectStmt(getAlbumComment.c_str())[0][0], pic.getName(), user.getId());
	}

	for (auto album : getAlbumsOfUser(user))
	{
		this->deleteAlbum(album.getName(), user.getId());
	}
	

	this->deleteDataW("USERS", "ID=" + std::to_string(user.getId()) + " AND NAME='" + user.getName() + "'");
}
/*
Check if user exist
Input: ID
Output: If exist
*/
bool IDataAccess::doesUserExists(int userId)
{
	return this->callCommentUser("select * from users where id = " + std::to_string(userId) + ";").size() != 0;
}
/*
Count albums of user
Input: User
Output: Number of albums
*/
int IDataAccess::countAlbumsOwnedOfUser(const User& user)
{
	return this->getAlbumsOfUser(user).size();
}
/*
How mach many tags user have in all his albums
Input: User
Output: Number of tags
*/
int IDataAccess::countAlbumsTaggedOfUser(const User& user)
{
	int numberTags = 0;
	
	for (Album album : this->getAlbumsOfUser(user))
	{
		string getPictureCommnet = "select * from pictures WHERE album_id =" + std::to_string(album.getOwnerId()) + ";";

		Records albumPictureRecords = selectStmt(getPictureCommnet.c_str());
		
		for (Record picRecord : albumPictureRecords)
		{
			// count the tags of this pic

			string CountTagsComment = "select count(*) from TAGS where picture_id=" + picRecord[0] + ";";

			// add to numbers of tags
			string numStr = selectStmt(CountTagsComment.c_str())[0][0];
			numberTags += std::stoi(numStr);

			// continue to next pic
		}
		
		// continue to next album
	}

	return numberTags;
}

/*
How mach tags user have been tag
Input: User
Output: Number of tags
*/
int IDataAccess::countTagsOfUser(const User& user)
{
	string howManyTagsComment = "select count(*) from TAGS where user_id=" + std::to_string(user.getId()) + ";";
	string numStr = selectStmt(howManyTagsComment.c_str())[0][0];
	return std::stoi(numStr);
}
/*
Average tags per album of user
Input: User
Output: Average
*/
float IDataAccess::averageTagsPerAlbumOfUser(const User& user)
{
	auto albumNumber = countAlbumsOwnedOfUser(user);
	if (albumNumber)
		return countAlbumsTaggedOfUser(user) / albumNumber;
	return -1; // if have zero
}
/*
Get top tagged user
Input: None
Output: The user
*/
User IDataAccess::getTopTaggedUser()
{
	User mostTagUser = User(-1, "No have most liked");
	int mostTagsNum = -1;

	string mostTagedUserComment = "select user_id from TAGS GROUP BY user_id ORDER BY count(user_id) DESC;";

	string numStr = selectStmt(mostTagedUserComment.c_str())[0][0];

	return this->getUser(std::stoi(numStr));
}
/*
Save return answers from DB to the first pointer, use for execSQLlite3
Input: First value is Records pointer... input of execSQLlite3 
Output: 0 if not fail 1 else
*/
int IDataAccess::selectCallback(void* dataPointer, int numFields, char** pFields, char** pColNames)
{
	Records* records = static_cast<Records*>(dataPointer);  // casting from the void pointer to Records class
	try 
	{
		records->emplace_back(pFields, pFields + numFields);  // add the data
	}
	catch (...) 
	{
		// abort select on failure, don't let exception propogate thru sqlite3 call-stack
		return 1;
	}
	return 0;
}
/*
Take pic from given Record
Input: Record of pic
Output: Picture
*/
Picture IDataAccess::recToPic(Record rec)
{
	return Picture(stoi(rec[0]), rec[1], rec[2], rec[3], rec[4]);
}
/*
Get top tagged picture
Input: None
Output: Picture
*/
Picture IDataAccess::getTopTaggedPicture()
{
	
	string mostTagedUserComment = "select picture_id from TAGS GROUP BY picture_id ORDER BY count(picture_id) DESC;";

	string numStr = selectStmt(mostTagedUserComment.c_str())[0][0];

	string getPicComment = "select * from pictures where id=" + numStr + ";";

	Record picRecord = selectStmt(getPicComment.c_str())[0];

	return recToPic(picRecord);
}
/*
Get the picturs of where the user have been taged
Input: The User
Output: Pictures
*/
std::list<Picture> IDataAccess::getTaggedPicturesOfUser(const User& user)
{
	auto pics = std::list<Picture>();
	// need to show dups
	string getPicsComment = "select * from TAGS where user_id=" + std::to_string(user.getId()) + ";";
	
	for (Record record : selectStmt(getPicsComment.c_str()))
	{
		auto getPicComment = "select * from PICTURES where id =" + record[1] + ";";

		auto picData = selectStmt(getPicComment.c_str());

		if (picData.size() != 0)
			pics.push_back(recToPic(picData[0]));
	}

	return pics;
}
/*
Return the answers of DB
Input: The coommet to the DB
Output: The return Records
*/
Records IDataAccess::selectStmt(const char* comment)
{
	Records records;
	char* errorMsg;
	int ret = sqlite3_exec(this->_db, comment, IDataAccess::selectCallback, &records, &errorMsg);
	if (ret != SQLITE_OK)
	{
		std::cerr << "Error in select statement " << comment << "[" << errorMsg << "]\n";
	}
	else 
	{
		//std::cerr << records.size() << " records returned.\n";
	}

	return records;
}

